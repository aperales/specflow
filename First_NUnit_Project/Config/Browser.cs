﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Safari;

namespace First_NUnit_Project.Config
{
    class Browser
    {
        public enum BrowserConfig
        {
            InternetExplorer,
            GoogleChrome,
            FireFox,
            MicrosoftEdge,
            Safari
        }

        public enum Environment
        {
            localhost,       
            dev,
            qa,
            uat,
            prod
        }

        public static BrowserConfig browser = BrowserConfig.GoogleChrome;
        public static Environment environment = Environment.localhost;
        public static IWebDriver WebDriver;

        public static string baseUrl;

        public static void GoTo(string url)
        {
            WebDriver.Url = baseUrl + url;
        }

        public static void Init()
        {
            switch (environment)
            {
                case Environment.localhost:
                    baseUrl = "http://store.demoqa.com/";
                    break;
                case Environment.dev:
                    baseUrl = "http://toolsqa.com/automation-practice-table/";
                    break;
                case Environment.qa:
                    baseUrl = "http://store.demoqa.com/";
                    break;
                case Environment.uat:
                    baseUrl = "http://store.demoqa.com/";
                    break;
                case Environment.prod:
                    baseUrl = "http://store.demoqa.com/";
                    break;
                default:
                    baseUrl = string.Empty;
                    break;
            }

            switch (browser)
            {
                case BrowserConfig.InternetExplorer:
                    var IEoptions = new InternetExplorerOptions
                    {
                        EnableNativeEvents = true,
                        IntroduceInstabilityByIgnoringProtectedModeSettings =
                                                true,
                        IgnoreZoomLevel = true,
                        EnsureCleanSession = true,
                        RequireWindowFocus = false
                    };
                    WebDriver = new InternetExplorerDriver(IEoptions);
                    WebDriver.Manage().Window.Maximize();
                    break;
                case BrowserConfig.GoogleChrome:
                    WebDriver = new ChromeDriver();
                    WebDriver.Manage().Window.Maximize();
                    break;
                case BrowserConfig.FireFox:
                    WebDriver = new FirefoxDriver();
                    WebDriver.Manage().Window.Maximize();
                    break;
                case BrowserConfig.Safari:
                    WebDriver = new SafariDriver();
                    WebDriver.Manage().Window.Maximize();
                    break;
                default:
                    IEoptions = new InternetExplorerOptions
                    {
                        EnableNativeEvents = true,
                        IntroduceInstabilityByIgnoringProtectedModeSettings = true,
                        IgnoreZoomLevel = true,
                        EnsureCleanSession = true,
                        RequireWindowFocus = false
                    };
                    WebDriver = new InternetExplorerDriver(IEoptions);
                    WebDriver.Manage().Window.Maximize();
                    break;
            }

            WebDriver.Url = baseUrl;
        }

        public static void Quit()
        {
            Console.WriteLine("Closing Browser...");
            WebDriver.Close();
            WebDriver.Quit();
        }
    }
}
