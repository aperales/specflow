﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace First_NUnit_Project.Config
{
    public class ExcelUtil
    {
        private static List<DataCollection> dataCol = new List<DataCollection>();

        private static DataTable ExcellToDataTable(string fileName)
        {
            try
            {
                FileStream stream = File.Open(fileName, FileMode.Open, FileAccess.Read);
                IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                var conf = new ExcelDataSetConfiguration
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = true
                    }
                };
                DataSet result = excelReader.AsDataSet(conf);
                DataTableCollection table = result.Tables;
                DataTable resultTable = table["Sheet1"];

                return resultTable;
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
                return null;
            }
        }

        public static void PopulateInCollection(string fileName)
        {
            DataTable table = ExcellToDataTable(fileName);

            for (int row = 1; row <= table.Rows.Count; row++)
            {
                for (int col = 0; col < table.Columns.Count; col++)
                {
                    DataCollection dtTable = new DataCollection()
                    {
                        rowNumber = row,
                        colName = table.Columns[col].ColumnName,
                        colValue = table.Rows[row - 1][col].ToString()
                    };
                    dataCol.Add(dtTable);
                }
            }
        }

        public static string ReadData(int rowNumber, string columnName)
        {
            try
            {
                //string data = (from colData in dataCol
                //               where colData.colName == columnName && colData.rowNumber == rowNumber
                //               select colData.colValue).SingleOrDefault();
                var data = dataCol.Where(x => x.colName.Equals(columnName) && x.rowNumber == rowNumber).SingleOrDefault().colValue;
                return data.ToString();
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }


    }

    public class DataCollection
    {
        public int rowNumber { get; set; }
        public string colName { get; set; }
        public string colValue { get; set; }
    }

}
