﻿using NUnit.Framework;
using System;
using OpenQA.Selenium.Remote;
using First_NUnit_Project.Config;
using TechTalk.SpecFlow;
//using First_NUnit_Project.PageObjects;

namespace First_NUnit_Project
{
    [Binding]
    public class Set_Up
    {
        //public static IWebDriver driver;
        DesiredCapabilities capabilities = new DesiredCapabilities();

        [BeforeTestRun]
        [SetUp]
        public static void SetUp()
        {
            Console.WriteLine("Opening Browser...");
            Browser.browser = Browser.BrowserConfig.GoogleChrome;
            Browser.environment = Browser.Environment.dev;
            Browser.Init();
            ExcelUtil.PopulateInCollection(@"c:\tmp\store.xlsx");
        }

        [AfterTestRun]
        [TearDown]
        public static void CloseBrowser()
        {
            Browser.WebDriver.Close();
            Browser.WebDriver.Quit();
        }
    }
}
