﻿Feature: Home_Page
	In order to avoid silly mistakes
	As a manual tester
	I want to search for a product

@SmokeTest
Scenario Outline: Search for a product with EXCEL
	Given I have clicked the Home button
	And I have used Excel <Row> to search for a product
	Examples:
	| Row | 
    | 1   | 
	| 2   | 

Scenario Outline: Search for a product with iterations
	Given I have clicked the Home button
	And I have searched for <Product>

@source:data.xlsx
	Examples:
	| Product	  | 
    | iPod Nano   | 
	| Mac Mini    | 

