﻿Feature: Login_Page
    In order to avoid silly mistakes
	As a manual tester
	I want to search for a product

@SmokeTest
Scenario: Click the login link
	When I click the Log In link

Scenario: Do a Login with Excel
	Given I have used Excel to fill the login form
	When I press the Log In button
    Then an error message should be prompted

Scenario Outline: Do a Login with iterations
	Given I have filled the login form <User> and <Pwd>
	When I press the Log In button
    Then an error message should be prompted	
Examples:
	 | User	                       | Pwd	         | 
     | afner_guego@hotmail.com     | 123456          | 
	 | AFNER QA                    | password        | 
