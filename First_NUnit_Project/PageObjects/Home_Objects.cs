﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using First_NUnit_Project.Config;

namespace First_NUnit_Project.PageObjects
{
    public class Home_Objects 
    {

        public Home_Objects()
        {
            PageFactory.InitElements(Browser.WebDriver, this);
        }

        [FindsBy(How = How.CssSelector, Using = "#menu-item-15 > a")]
        public IWebElement HomeBtn { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id=\"menu - item - 33\"]/a")]
        public IWebElement CategoryBtn { get; set; }

        [FindsBy(How = How.Name, Using = "s")]
        public IWebElement SearchField { get; set; }

        [FindsBy(How = How.LinkText, Using = "Log in")]
        public IWebElement LoginLink { get; set; }

        /*public string Product { get; set; }*/

    }
}
