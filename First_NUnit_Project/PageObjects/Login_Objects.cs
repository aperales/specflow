﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using First_NUnit_Project.Config;

namespace First_NUnit_Project.PageObjects
{
    public class Login_Objects 
    {
        public Login_Objects()
        {
            PageFactory.InitElements(Browser.WebDriver, this);
        }

        [FindsBy(How = How.LinkText, Using = "Log in")]
        public IWebElement LoginLink { get; set; }

        [FindsBy(How = How.Id, Using = "user_login")]
        public IWebElement UserName { get; set; }

        [FindsBy(How = How.Id, Using = "user_pass")]
        public IWebElement Password { get; set; }

        [FindsBy(How = How.Id, Using = "wp-submit")]
        public IWebElement LoginBtn { get; set; }

        [FindsBy(How = How.Id, Using = "login_error")]
        public IWebElement LoginError { get; set; }

        /*public string User { get; set; }

        public string Pwd { get; set; }*/
    }
}
