﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using First_NUnit_Project.Config;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace First_NUnit_Project.PageObjects
{
    class Table_Objects
    {
        public Table_Objects()
        {
            PageFactory.InitElements(Browser.WebDriver, this);
        }


        [FindsBy(How = How.LinkText, Using = "details")]
        public IWebElement DetailsLink { get; set; }

        //public int numofLinks { get; set; }

        static int numofLinks = Browser.WebDriver.FindElements(By.LinkText("details")).Count;

        static string[,] array2D = new string[numofLinks, 7];


        public static void Matrix()
        {
            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                for (int j = 0; j < array2D.GetLength(1); j++)
                {
                    if (j == 0)
                    {
                        array2D[i, j] = Browser.WebDriver.FindElement(By.XPath("//*[@id=\"content\"]/table/tbody/tr[" + (i+1) + "]/th")).Text;
                        Console.WriteLine(array2D[i, j]);
                    }
                    else
                    {
                        array2D[i, j] = Browser.WebDriver.FindElement(By.XPath("//*[@id=\"content\"]/table/tbody/tr[" + (i + 1) + "]/td[" + j + "]")).Text;
                        Console.WriteLine(array2D[i, j]);
                    }
                }
            }

        }

        //decimal numOfRows = Browser.WebDriver.FindElements(By.XPath("/html/body/..../table/tbody/tr")).Count;
    }
}
