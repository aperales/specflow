﻿using First_NUnit_Project.Config;
using First_NUnit_Project.PageObjects;
using OpenQA.Selenium;

namespace First_NUnit_Project.Pages
{
    class Home_Page
    {
        static Home_Objects Object = new Home_Objects();
        public static void buscandoanemo()
        {
            Object.HomeBtn.Click();
            Object.SearchField.SendKeys("iPod Nano");
            Object.SearchField.SendKeys(Keys.Enter);
            Object.LoginLink.Click();
        }

        public static void Search(int row)
        {
            string product = (ExcelUtil.ReadData(row, "Product"));
            Object.SearchField.SendKeys(product);
            Object.SearchField.SendKeys(Keys.Enter);
        }
    }
}
