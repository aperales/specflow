﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using First_NUnit_Project.Config;
using First_NUnit_Project.PageObjects;
using NUnit.Framework;

namespace First_NUnit_Project.Pages
{
    class Login_Page
    {
        static Login_Objects Object = new Login_Objects();
        public static void login()
        {
            Object.UserName.SendKeys("aperales@sciodev.com");
            Object.Password.SendKeys("123456");
            Object.LoginBtn.Click();
            Assert.True(Object.LoginError.Displayed);
        }

        public static void loginwithExcel()
        {
            Object.UserName.SendKeys(ExcelUtil.ReadData(1, "User"));
            Object.Password.SendKeys(ExcelUtil.ReadData(1, "Password"));
        }
    }
}
