﻿using System;
using First_NUnit_Project.PageObjects;
using First_NUnit_Project.Pages;
using TechTalk.SpecFlow;

namespace First_NUnit_Project.StepDefinitions
{
    [Binding]
    public class Table_PageSteps
    {
        static Table_Objects Object = new Table_Objects();

        [Given(@"I have counted the number of links")]
        public void GivenIHaveCountedTheNumberOfLinks()
        {
            Table_Objects.Matrix();
            //Table_Page.Count_table();
        }
    }
}
