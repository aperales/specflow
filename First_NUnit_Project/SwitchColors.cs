﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace First_NUnit_Project
{
    [TestFixture]
    public class SwitchColors
    {
        IWebDriver driver;
        DesiredCapabilities capabilities = new DesiredCapabilities();

        [SetUp]
        public void SetUp()
        {
            Console.WriteLine("Opening Browser...");
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            // Created object of DesiredCapabilities class.


            // Set android deviceName desired capability. Set your device name.
            //capabilities.setCapability("deviceName", "your Device Name");

            // Set BROWSER_NAME desired capability.
            capabilities.SetCapability(CapabilityType.BrowserName, "AfnerBrowser");
        }

        /*[Test]
        public void Colors()
        {
            Console.WriteLine("Running Test Case...");
            // TODO: Add your test code here
            driver.Url = "http://toolsqa.com/automation-practice-switch-windows/";

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            Func<IWebDriver, IWebElement> waitForElement = new Func<IWebDriver, IWebElement>((IWebDriver Web) =>
            {
                Console.WriteLine("Waiting for color to change");
                IWebElement element = Web.FindElement(By.Id("colorVar"));
                if (element.GetAttribute("style").Contains("red"))
                {
                    return element;
                }
                return null;
            });

            IWebElement targetElement = wait.Until(waitForElement);
            Console.WriteLine("The color of the button is " + targetElement.GetAttribute("innerHTML"));



            var clock = driver.FindElement(By.Id("clock"));
            wait.Until(ExpectedConditions.TextToBePresentInElement(clock, "Buzz Buzz"));
            //colorvar.Click();
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            //WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            //wait.Until(ExpectedConditions.ElementIsVisible(By.Id("colorVar")));

            //wait.Until(ExpectedConditions.ElementIsVisible(By.Id("colorVar[contains(@style, 'color: red')]")));

        }*/

        [TearDown]
        public void CloseBrowser()
        {
            Console.WriteLine("Closing Browser...");
            driver.Close();
            driver.Quit();
        }
    }
}

