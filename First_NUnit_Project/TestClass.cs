﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace First_NUnit_Project
{
    [TestFixture]
    public class TestClass
    {
        IWebDriver driver;

        [SetUp]
        public void SetUp()
        {
            Console.WriteLine("Opening Browser...");
            driver = new FirefoxDriver();
            
        }

        [Test]
        public void TestMethod()
        {
            Console.WriteLine("Running Test Case...");
            // TODO: Add your test code here
            driver.Url = "http://toolsqa.com/automation-practice-form/  ";
            //Assert.Pass("Your first passing test");
            driver.FindElement(By.Name("firstname")).SendKeys("Angie");
            driver.FindElement(By.Name("lastname")).SendKeys("Sensei");
            driver.FindElement(By.Id("sex-1")).Click();
            driver.FindElement(By.Id("exp-2")).Click();
            driver.FindElement(By.Id("datepicker")).SendKeys("2015-09-01");
            driver.FindElement(By.Id("profession-0")).Click();
            driver.FindElement(By.Id("profession-1")).Click();
            driver.FindElement(By.Id("tool-1")).Click();
            driver.FindElement(By.Id("tool-2")).Click();
            driver.FindElement(By.Id("continents")).SendKeys(Keys.Enter);
            driver.FindElement(By.Id("continents")).SendKeys("N");
            driver.FindElement(By.Id("continents")).SendKeys(Keys.Enter);
        }

        [TearDown]
        public void CloseBrowser()
        {
            Console.WriteLine("Closing Browser...");
            driver.Close();
        }
    }
}
