﻿using System;
using TechTalk.SpecFlow;
//using SpecFlow_Calculator.StepDefinitions;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using First_NUnit_Project.PageObjects;
using OpenQA.Selenium;
using First_NUnit_Project.Pages;

namespace First_NUnit_Project
{
    [Binding]
    public class Home_PageSteps
    {
        static Home_Objects Object = new Home_Objects();

        [Given(@"I have clicked the Home button")]
        public void GivenIHaveClickedTheHomeButton()
        {
            Object.HomeBtn.Click();
        }

        [Given(@"I have searched for (.*)")]
        public void GivenIHaveSearchedFor(string product)
        {
            Object.SearchField.SendKeys(product);
            Object.SearchField.SendKeys(Keys.Enter);
        }

        [Given(@"I have used Excel (.*) to search for a product")]
        public void GivenIHaveUsedExcelToSearchForAProduct(int row)
        {
            Home_Page.Search(row);
        }
        
        [When(@"I click the Login link")]
        public void WhenIClickTheLoginLink()
        {
            Object.LoginLink.Click();
        }
        
        [Then(@"the page should be Login")]
        public void ThenThePageShouldBeLogin()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
