﻿using System;
using TechTalk.SpecFlow;
using First_NUnit_Project.PageObjects;
using OpenQA.Selenium;
using NUnit.Framework;
using TechTalk.SpecFlow.Assist;
using First_NUnit_Project.Pages;

namespace First_NUnit_Project
{
    [Binding]
    public class Login_PageSteps
    {
        static Login_Objects Object = new Login_Objects();
        [When(@"I click the Log In link")]
        public void WhenIClickTheLogInLink()
        {
            Object.LoginLink.Click();
        }

        [Given(@"I have filled the login form (.*) and (.*)")]
        public void GivenIHaveFilledTheLoginFormAnd(string user, string pwd)
        {
            Object.UserName.SendKeys(user);
            Object.Password.SendKeys(pwd);
        }

        [Given(@"I have used Excel to fill the login form")]
        public void GivenIHaveUsedExcelToFillTheLoginForm()
        {
            Login_Page.loginwithExcel();
        }


        [Given(@"I have filled the login form")]
        public void GivenIHaveFilledTheLoginForm(Table table)
        {
            ScenarioContext.Current.Pending();

            /*var rows = table.CreateInstance<Login_Objects>();
            Object.UserName.SendKeys(rows.User);
            Object.Password.SendKeys(rows.Pwd);

            var rows = table.CreateSet<Login_Objects>();

            foreach (Login_Objects log in rows)
            {
                Object.UserName.SendKeys(log.User);
                Object.Password.SendKeys(log.Pwd);
            }*/
        }
        
        [When(@"I press the Log In button")]
        public void WhenIPressTheLogInButton()
        {
            Object.LoginBtn.Click();
        }
        
        [Then(@"an error message should be prompted")]
        public void ThenAnErrorMessageShouldBePrompted()
        {
            Assert.True(Object.LoginError.Displayed);
        }
    }
}
